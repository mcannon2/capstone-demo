package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class MetlifeController {

	@RequestMapping(value="/metlife")
	public String sayHello (Model model) {
		model.addAttribute("greeting", "Test");
			return "metlife";
	}
	
	
}
